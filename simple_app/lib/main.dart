import 'package:flutter/material.dart';
import 'bio.dart';

void main() => runApp(MaterialApp(
  home: Home(),
));


class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  int posts_counter = 0;
  List<Bio> datas = [
    Bio(article:"Massa", vendor:'Akoulchi'),
    Bio(article:"Tsalla", vendor:'Pavé darEssalam'),
    Bio(article:"Dambou", vendor:'Plateau')
  ];

  Widget bioTemplate(info){
    return Card(
      margin: EdgeInsets.fromLTRB(16, 16,16,0),
      child: Padding(
        padding: const EdgeInsets.all(12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
                info.article,
                style: TextStyle(
                  color:Colors.grey[600],
                  letterSpacing: 1,
                  fontSize: 18,
                ),
              ),
              SizedBox(height: 10,),
              Text(
                info.vendor,
                style: TextStyle(
                  color:Colors.grey[800],
                  letterSpacing: 1,
                  fontSize: 14,
                  fontWeight: FontWeight.bold
                ),
              ),
        ],
      ),
      ) 
      
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[900],
      appBar: AppBar(
        title: Text('Contact Card'),
        centerTitle: true,
        backgroundColor: Colors.grey[850],
        elevation: 0.0,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          setState(() {
            posts_counter+=1;
          });
        },
        child:Icon(
          Icons.add
        ),
        backgroundColor: Colors.lightGreen,
      ),
      body: Container (
        child: Padding(
        padding: EdgeInsets.fromLTRB(30,40,30,0),
        child:ListView(
          shrinkWrap: true,
          children: <Widget>[
            Center(
              child: CircleAvatar(
                backgroundImage: AssetImage('assets/img_snowtops.jpg'),
                radius: 40
              ),
            ),
            Divider(
              height: 100,
              color: Colors.grey[800]
            ),
            Text(
              'NAME',
              style: TextStyle(
                color: Colors.grey,
                letterSpacing: 2.0
              ),
            ),
            SizedBox(height: 10),
            Text(
              'Nadir',
              style: TextStyle(
                color:Colors.amberAccent[200],
                letterSpacing: 2,
                fontSize: 28,
                fontWeight: FontWeight.bold
              ),
            ),
            SizedBox(height: 30),
            Text(
              'Posts',
              style: TextStyle(
                color: Colors.grey,
                letterSpacing: 2.0
              ),
            ),
            SizedBox(height: 10),
            Text(
              '$posts_counter',
              style: TextStyle(
                color:Colors.amberAccent[200],
                letterSpacing: 2,
                fontSize: 28,
                fontWeight: FontWeight.bold
              ),
            ),
            SizedBox(height: 30),
            Text(
              'email',
              style: TextStyle(
                color: Colors.grey,
                letterSpacing: 2.0
              ),
            ),
            SizedBox(height: 10),
            Row(children: <Widget>[
              Icon(
                Icons.email,
                color:Colors.grey[500]
              ),
              SizedBox(width: 10,),
              Text(
              'nadir@gmail.com',
              style: TextStyle(
                color:Colors.grey[500],
                letterSpacing: 1,
                fontSize: 18,
                fontWeight: FontWeight.bold
              ),
            ),
            SizedBox(height: 30),
            ],),
            Divider(
              height: 10,
              color: Colors.grey[800]
            ),
            Text(
              'BIO',
              style: TextStyle(
                color: Colors.grey,
                letterSpacing: 2.0
              ),
            ),
            SizedBox(height: 20),
            Column(
              children: datas.map((data){
                return bioTemplate(data);
              }).toList(),
            ),
            SizedBox(height: 30),
          ],
        )
      )
      )
      
  );
  }
}